<?php
/*
Plugin Name:  Portfolio Post Type
Plugin URI:   https://anders.gressli.net/
Description:  Add portfolio post type
Version:      1.0.0
Author:       Anders Gressli
Author URI:   https: //anders.gressli.net/
Text Domain:  wporg
Domain Path:  /languages
License:      GPL2
 */

/*
* Creating a function to create our CPT
*/

function ag_setup_project_post_type()
{
    // Set UI labels for Custom Post Type
    $labels = array(
        'name' => _x('Projects', 'Post Type General Name', 'sage'),
        'singular_name' => _x('Project', 'Post Type Singular Name', 'sage'),
        'menu_name' => __('Projects', 'sage'),
        'parent_item_colon' => __('Parent Project', 'sage'),
        'all_items' => __('All Projects', 'sage'),
        'view_item' => __('View Project', 'sage'),
        'add_new_item' => __('Add New Project', 'sage'),
        'add_new' => __('Add New', 'sage'),
        'edit_item' => __('Edit Project', 'sage'),
        'update_item' => __('Update Project', 'sage'),
        'search_items' => __('Search Project', 'sage'),
        'not_found' => __('Not Found', 'sage'),
        'not_found_in_trash' => __('Not found in Trash', 'sage'),
    );

    // Set other options for Custom Post Type
    $args = array(
        'label' => __('Projects', 'sage'),
        'description' => __('Project news and reviews', 'sage'),
        'labels' => $labels,
        // Features this CPT supports in Post Editor
        'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields'),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies' => array(),
        /* A hierarchical CPT is like Pages and can have
         * Parent and child items. A non-hierarchical CPT
         * is like Posts.
         */
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'rewrite' => array('slug' => 'work'),
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );

    // Registering your Custom Post Type
    register_post_type('project', $args);

    register_taxonomy(
        'project_category',
        'project',
        array(
            'labels' => array(
                'name' => 'Project Categories',
                'add_new_item' => 'Add New project category',
                'new_item_name' => "New project category",
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true,
            'show_admin_column' => true,
        )
    );

    register_taxonomy(
        'project_tags',
        'project',
        array(
            'labels' => array(
                'name' => 'Project Tags',
                'add_new_item' => 'Add New project tag',
                'new_item_name' => "New project tag",
            ),
            'show_ui' => true,
            'update_count_callback' => '_update_post_term_count',
            'show_tagcloud' => false,
            'hierarchical' => true,
            'show_admin_column' => true,
            'query_var' => true,
        )
    );
}

/* Hook into the 'init' action so that the function
 * Containing our post type registration is not
 * unnecessarily executed.
 */

add_action('init', 'ag_setup_project_post_type', 0);

function ag_install()
{
    // trigger our function that registers the custom post type
    ag_setup_project_post_type();

    // clear the permalinks after the post type has been registered
    flush_rewrite_rules();
}

function ag_deactivation()
{
    // unregister the post type, so the rules are no longer in memory
    unregister_post_type('book');
    // clear the permalinks to remove our post type's rules from the database
    flush_rewrite_rules();
}
register_deactivation_hook(__FILE__, 'ag_deactivation');

add_filter('manage_edit-projects_columns', 'ag_project_columns');

function ag_project_columns($columns)
{
    unset($columns['date']);
    unset($columns['comments']);
    return $columns;
}


add_action('restrict_manage_posts', 'ag_project_filter_list');

function ag_project_filter_list()
{
    $screen = get_current_screen();
    global $wp_query;
    if ($screen->post_type == 'project') {
        wp_dropdown_categories(array(
            'show_option_all' => 'Show All project tags',
            'taxonomy' => 'project_tags',
            'name' => 'project_tags',
            'orderby' => 'name',
            'selected' => (isset($wp_query->query['project_tags']) ? $wp_query->query['project_tags'] : ''),
            'hierarchical' => false,
            'depth' => 3,
            'show_count' => false,
            'hide_empty' => false,
        ));
    }
}

add_filter('parse_query', 'perform_filtering');
function perform_filtering($query)
{
    $qv = &$query->query_vars;

    if ( isset($qv['project_tags']) ) {
        $term = get_term_by('id', $qv['project_tags'], 'project_tags');
        $qv['project_tags'] = $term->slug;
    }
}

